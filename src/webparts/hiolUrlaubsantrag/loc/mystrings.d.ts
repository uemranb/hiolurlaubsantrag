declare interface IHiolUrlaubsantragWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'HiolUrlaubsantragWebPartStrings' {
  const strings: IHiolUrlaubsantragWebPartStrings;
  export = strings;
}

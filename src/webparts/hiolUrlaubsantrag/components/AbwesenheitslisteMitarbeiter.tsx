import { DetailsList, IColumn } from "office-ui-fabric-react";
import * as React from "react";
import { Abwesenheiten, Abwesenheitsservice } from "../service/AbwesenheitsService";

interface AbwesenheitsListeMitarbeiter{
    
}

const columns: IColumn[] = [
    {key: "column1", name: "Mitarbeiter", fieldName:"Mitarbeiter", minWidth: 100, maxWidth: 300, isResizable: true },
    {key: "column1", name: "Startdatum", fieldName:"Startdatum", minWidth: 100, maxWidth: 300, isResizable: true },
    {key: "column1", name: "Enddatum", fieldName:"Enddatum", minWidth: 100, maxWidth: 300, isResizable: true }
];
export default function AbwesenheitsListeMitarbeiter(props: AbwesenheitsListeMitarbeiter){
    const[abwesenheiten, setAbwesenheiten] = React.useState<Abwesenheiten[] | null>(null);
    React.useEffect(() => {
        Abwesenheitsservice.getMaAbwesenheiten().then(data => {
            setAbwesenheiten(data);
        });
    }, []); //leere Liste verhindert mehrere Requests und führt es einmalig aus. 

    return (
      <>
       {abwesenheiten !== null && <DetailsList
        items={abwesenheiten}
        columns={columns}
        />}
      </>
    );
  }
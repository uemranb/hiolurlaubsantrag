import * as React from 'react';
import styles from './HiolUrlaubsantrag.module.scss';
import { IHiolUrlaubsantragProps } from './IHiolUrlaubsantragProps';
import { escape } from '@microsoft/sp-lodash-subset';
import AbwesenheitsListeMitarbeiter from './AbwesenheitslisteMitarbeiter';

export default class HiolUrlaubsantrag extends React.Component<IHiolUrlaubsantragProps, {}> {
  public render(): React.ReactElement<IHiolUrlaubsantragProps> {
    return (
    <div>
      <AbwesenheitsListeMitarbeiter></AbwesenheitsListeMitarbeiter>
    </div>
    );
  }
}

import { sp } from '@pnp/sp';
import '@pnp/sp/forms';
import '@pnp/sp/lists';
import '@pnp/sp/webs';
import '@pnp/sp/lists';
import '@pnp/sp/items';
import { format } from 'date-fns';
import { de } from 'date-fns/locale';

export interface Abwesenheiten{
   Mitarbeiter: string;
   Startdatum:string;
   Enddatum: string;
   Status: string;
   Urlaubstyp: string;
   Anzahl: number;
   Vertretung: string;
   Kommentar: string;
}

export class Abwesenheitsservice {
	public static async getMaAbwesenheiten(): Promise<Abwesenheiten[]> {
		sp.setup({
			sp: {
				headers: {
					Accept: 'application/json; odata=verbose',

					'Content-Type': 'application/json; odata=verbose'
				},
				baseUrl: 'https://hanseinstitut.sharepoint.com/sites/Urlaubsantrag'
			}
		});

		const allItems = await sp.web.lists.getByTitle('Abwesenheiten').items.select("Mitarbeiter/Benutzername, Startdatum, Enddatum").expand("Mitarbeiter").filter("Status eq 'Genehmigt'").getAll();

		console.log(allItems);

		const abwesenheiten: Abwesenheiten[] = allItems.map((item) => {
			return {  Mitarbeiter: item['Mitarbeiter']['Benutzername'], 
                      Startdatum: format(new Date(item['Startdatum']),'dd.MM.yyyy', {locale: de}),
                      Enddatum: format(new Date(item['Enddatum']),'dd.MM.yyyy', {locale: de})
        }as Abwesenheiten;
		});

		return abwesenheiten;
	}
}
